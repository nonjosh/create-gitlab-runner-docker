docker volume create gitlab-gpu-runner-config
docker run -d --name gitlab-gpu-runner --restart always \
    --gpus all \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-gpu-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest