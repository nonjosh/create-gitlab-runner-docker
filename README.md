# create-gitlab-runner-docker

## How to create GitLab runner and register with docker

start container

```sh
docker volume create gitlab-runner-config
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

register

```sh
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register
```

### enable docker-in-docker

The enable docker-in-docker some changes to the gitlab runner configuration are required.

```sh
cat << EOF | sudo patch /etc/gitlab-runner/config.toml
--- /etc/gitlab-runner/config.toml
+++ /etc/gitlab-runner/config.toml
@@ -9,15 +9,16 @@
   url = "https://gitlab.com/"
   token = "xyzXYZxyzXYZ"
   executor = "docker"
+  environment = ["DOCKER_TLS_CERTDIR=/certs","DOCKER_DRIVER=overlay2"]
   [runners.custom_build_dir]
   [runners.docker]
     tls_verify = false
     image = "alpine:latest"
-    privileged = false
+    privileged = true
     disable_entrypoint_overwrite = false
     oom_kill_disable = false
     disable_cache = false
-    volumes = ["/cache"]
+    volumes = ["/certs/client", "/cache"]
     shm_size = 0
   [runners.cache]
     [runners.cache.s3]
EOF
```

## References

- [https://youtu.be/jUiKi6FWYrg]

- [https://docs.gitlab.com/runner/install/docker.html]

- [https://docs.gitlab.com/runner/register/index.html#docker]

- [https://brainfood.xyz/post/20191116-setup-gitlab-runner-with-docker-in-docker/]
